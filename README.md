We need a remote backup, with incremental features, that must be automated to save docker infrastructure.

The remote access has to be secure but without shell access:
- FTP weak and slow
- SCP/SFTP slow
- SSH grant a shell access
- WebDAV slow
- S3 :point_left:

Solution: [Restic] + [MinIO]

We deploy a server somewhere on Internet with a [MinIO server][minio-dc] (could be a docker, of course :grin:) with a bucket for every infrastructure, then create some scripts to automatize the backup proces.

[minio-dc]: (https://docs.min.io/docs/deploy-minio-on-docker-compose)

:exclamation::exclamation: Warning :exclamation::exclamation:

All the literature found on Internet says backup databases by dumping all DB on a text file. 
But we don't like it.

We stop the DB container then backup DB files and restart again. It's not the usual way but it works

Copy on every stack the backup directory and launch `backup/backup.sh` script to backup the stack.

```console
test_backup
├── docker-compose.yml
├── .env
├── backup
│   ├── backup.sh
│   ├── excludes.txt
│   ├── load_vars.sh
│   ├── restore.sh
│   └── unload_vars.sh
└── volumes
    ├── backup
    ├── config-omnidb
    └── database
```

The script has to be launch from the stack root because it finds there the `.env` file with configuracion variables and with a user that could read DB files (`docker`?)

On the `.env` file you must add Restic env. vars:

```bash
# Backup variables
AWS_ACCESS_KEY_ID="supersecretpassword"
AWS_SECRET_ACCESS_KEY="supersecretpassword"
RESTIC_REPOSITORY="s3:http://84.88.95.14:9000/backups"
RESTIC_PASSWORD="supersecretpassword"
```


[MinIO]: https://min.io "MinIO"
[Restic]: https://restic.net "Restic"
[ResticDoc]: https://restic.readthedocs.io "Restic documentation"

# Software

## Restic

[Restic Documentation][ResticDoc]

Restic uses environmental variables to acces to passwords and parameters:

```bash
export AWS_ACCESS_KEY_ID=Supersecretpassword
export AWS_SECRET_ACCESS_KEY=Supersecretpassword
export RESTIC_REPOSITORY=s3:http://192.168.168.241:9000/backups
```

## MinIO

[MinIO] is a high performance, distributed object storage system. It is software-defined, runs on industry standard hardware and is 100% open source under the Apache V2 license.


# Procedure

We'll use the `.env` docker-compose file to store Restic and MinIO variables. Check paths

### Initialization

Manualy intialize the repository

```bash
restic -r s3:http://192.168.168.241:32769/backups init
````
With this command `$RESTIC_REPOSITORY` not work :confused:

### Manual Backup

To correctly backup DB files, it has to be done by root or an user with access. Verify files owner.

```bash
restic -r s3:http://192.168.168.241:32769/backups --verbose backup /path/to/folder/to/backup
```

### Check Snapshots

Regular way
```
restic -r s3:http://192.168.168.241:32769/backups snapshots
```
our way:
```bash
. backup/load_vars.sh
restic snapshots
```
:exclamation: caution with the `.` before the path, it's necessary to export variables out of script context.


## Automatic Backup

### Configure cron

```cron
MAILTO="satan@inferno.org"
13 4 * * * cd /path/to/backup && ./fullbackup.sh
```

Configure snapshot purge:

TODO: correct this part
```cron
MAILTO="satan@inferno.org"
43 4 * * * cd /path/to/backup && . backup/load_vars.sh && /usr/bin/restic forget --keep-daily 7 --keep-weekly 5
```

```console
 marck ~ backup/backup.sh 
[+] Backup started
[+] Backuping /home/marck/test_backup
Vars Loaded
[+] Stoping containers
Stopping bkp-test_db_1_f5f25bdbe47b ... done
Pausing bkp-test_admin_1_16752df831ed ... done
repository 8cf481b2 opened successfully, password is correct

Files:           1 new,     7 changed,   205 unmodified
Dirs:            0 new,     2 changed,     0 unmodified
Added to the repo: 2.958 MiB

processed 213 files, 267.363 MiB in 0:03
snapshot fed6499e saved
[+] Re-starting Containers
Unpausing bkp-test_admin_1_16752df831ed ... done
Starting bkp-test_db_1_f5f25bdbe47b ... done
Starting bkp-test_omni_1_971c93da24d3 ... 
Starting bkp-test_omni_1_971c93da24d3 ... done
[+] Unseting env. variables

```

```console
marck ~ restic snapshots
ID        Time                 Host        Tags        Paths
------------------------------------------------------------------------------
04822ea4  2020-02-23 13:56:26  vubu19-10   BKP-Test    /test_backup
8367753a  2020-02-23 14:01:09  vubu19-10   BKP-Test    /test_backup
a305986c  2020-02-23 14:23:54  vubu19-10   BKP-Test    /test_backup
65db2cd1  2020-02-23 14:25:24  vubu19-10   BKP-Test    /test_backup
0fcbd870  2020-02-23 15:26:21  vubu19-10   BKP-Test    dockers/test_backup1
6b95532c  2020-02-23 15:27:03  vubu19-10   BKP-Test    dockers/test_backup1
3e3d59be  2020-02-23 15:27:13  vubu19-10   BKP-Test2   dockers/test_backup
------------------------------------------------------------------------------

```



[MinIO]: https://min.io "MinIO"
[Restic]: https://restic.net "Restic"
[ResticDoc]: https://restic.readthedocs.io "Restic documentation"


# Multi-stack backup

There's a special script `full_backup.sh` on the parent directory to backup **all stacks**.

This script looks on every directory a file named `backup/backup.sh` if's there it executes locally.


```console
dockers
├── full_backup.sh
├── no_backup
│   └── do_no_backup_this_diretory.txt
├── test_backup
│   ├── backup
│   │   ├── backup.sh
│   │   ├── column_2c_dat.sql
│   │   ├── excludes.txt
│   │   ├── load_vars.sh
│   │   ├── restore.sh
│   │   └── unload_vars.sh
│   ├── docker-compose.yml
│   └── volumes
│       ├── backup
│       ├── config-omnidb
│       └── database
└── test backup1
    ├── backup
    │   ├── backup.sh
    │   ├── column_2c_dat.sql
    │   ├── excludes.txt
    │   ├── load_vars.sh
    │   ├── restore.sh
    │   └── unload_vars.sh
    ├── docker-compose.yml
    └── volumes
        ├── backup
        ├── config-omnidb
        └── database
```

```console
marck /home/marck/dockers# ./full_backup.sh 
[+] Starting Backup for multiple Stacks
================================================================================
[-] Entering /home/marck/dockers/test_backup1

================================================================================[+] Backup started
[+] Backuping /home/marck/dockers/test_backup1
Vars Loaded
[+] Stoping containers
Stopping bkp-test_db_1_f5f25bdbe47b ... done
Pausing bkp-test_admin_1_16752df831ed ... done
repository 8cf481b2 opened successfully, password is correct

Files:           0 new,     5 changed,   208 unmodified
Dirs:            0 new,     3 changed,     0 unmodified
Added to the repo: 2.956 MiB

processed 213 files, 267.363 MiB in 0:02
snapshot 78403984 saved
[+] Re-starting Containers
Unpausing bkp-test_admin_1_16752df831ed ... done
Starting bkp-test_db_1_f5f25bdbe47b ... done
bkp-test_admin_1_16752df831ed is up-to-date
Starting bkp-test_omni_1_971c93da24d3 ... done
[+] Unseting env. variables

[+] Leaving /home/marck/dockers/test_backup1
================================================================================
[-] Entering /home/marck/dockers/test_backup

================================================================================[+] Backup started
[+] Backuping /home/marck/dockers/test_backup
Vars Loaded
[+] Stoping containers
Stopping bkp-test2_db_1_13965068af91 ... done
Pausing bkp-test2_admin_1_9e3580edd4ce ... done
repository 8cf481b2 opened successfully, password is correct

Files:           0 new,     5 changed,   208 unmodified
Dirs:            0 new,     3 changed,     0 unmodified
Added to the repo: 1.468 KiB

processed 213 files, 267.363 MiB in 0:01
snapshot 03a885b1 saved
[+] Re-starting Containers
Unpausing bkp-test2_admin_1_9e3580edd4ce ... done
Starting bkp-test2_db_1_13965068af91 ... done
Starting bkp-test2_omni_1_34f93d9159c5 ... 
Starting bkp-test2_omni_1_34f93d9159c5 ... done
[+] Unseting env. variables

[+] Leaving /home/marck/dockers/test_backup
================================================================================
[+] Backuped 3 Stacks
[+] Unseting env. variables (again) for the flies
================================================================================
```
